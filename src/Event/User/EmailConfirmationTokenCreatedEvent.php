<?php

declare(strict_types=1);

namespace App\Event\User;

use App\Entity\Security\EmailConfirmationToken;
use Symfony\Contracts\EventDispatcher\Event;

class EmailConfirmationTokenCreatedEvent extends Event
{
    /**
     * @var EmailConfirmationToken
     */
    private EmailConfirmationToken $emailConfirmationToken;

    public function __construct(EmailConfirmationToken $emailConfirmationToken)
    {
        $this->emailConfirmationToken = $emailConfirmationToken;
    }

    /**
     * @return EmailConfirmationToken
     */
    public function getToken(): EmailConfirmationToken
    {
        return $this->emailConfirmationToken;
    }
}
