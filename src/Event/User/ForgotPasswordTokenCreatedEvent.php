<?php

declare(strict_types=1);

namespace App\Event\User;

use App\Entity\Security\ForgotPasswordToken;
use Symfony\Contracts\EventDispatcher\Event;

class ForgotPasswordTokenCreatedEvent extends Event
{
    /**
     * @var ForgotPasswordToken
     */
    private ForgotPasswordToken $forgotPasswordToken;

    public function __construct(ForgotPasswordToken $forgotPasswordToken)
    {
        $this->forgotPasswordToken = $forgotPasswordToken;
    }

    /**
     * @return ForgotPasswordToken
     */
    public function getToken(): ForgotPasswordToken
    {
        return $this->forgotPasswordToken;
    }
}
