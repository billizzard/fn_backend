<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;

abstract class AbstractFixture extends Fixture
{
    /**
     * @param string $className
     * @param string $name
     *
     * @return string
     */
    public function generateReferenceName(string $className, string $name): string
    {
        return sprintf('%s-%s', $className, $name);
    }
}
