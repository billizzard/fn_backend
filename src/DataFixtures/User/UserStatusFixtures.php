<?php

declare(strict_types=1);

namespace App\DataFixtures\User;

use App\DataFixtures\LookupFixture;
use App\Entity\Security\UserStatus;

class UserStatusFixtures extends LookupFixture
{
    /**
     * @return string
     */
    protected function getLookupClass(): string
    {
        return UserStatus::class;
    }
}
