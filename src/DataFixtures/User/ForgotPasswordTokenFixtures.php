<?php

declare(strict_types=1);

namespace App\DataFixtures\User;

use App\DataFixtures\AbstractFixture;
use App\Entity\Security\ForgotPasswordToken;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ForgotPasswordTokenFixtures extends AbstractFixture implements DependentFixtureInterface
{
    public const TOKENS = [
        [
            'user' => 'forgot_password_user@example.com',
            'token' => 'token123',
        ],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::TOKENS as $details) {
            /** @var User $user */
            $user = $this->getReference($this->generateReferenceName(
                User::class,
                $details['user']
            ));

            $token = new ForgotPasswordToken($user);
            $token->setToken($details['token']);

            $manager->persist($token);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
