<?php

declare(strict_types=1);

namespace App\DataFixtures\User;

use App\DataFixtures\AbstractFixture;
use App\Entity\Security\UserStatus;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends AbstractFixture implements DependentFixtureInterface
{
    public const USERS = [
        'auth_user1@example.com' => [
            'password' => 'q1w2e3Q!W@E#',
            'status' => UserStatus::ACTIVE,
        ],
        'await_user1@example.com' => [
            'password' => 'q1w2e3Q!W@E#',
            'status' => UserStatus::AWAITING_CONFIRMATION,
        ],
        'forgot_password_user@example.com' => [
            'password' => 'q1w2e3Q!W@E#',
            'status' => UserStatus::ACTIVE,
        ],
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::USERS as $email => $details) {
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->encoder->encodePassword($user, $details['password']));

            /** @var UserStatus $status */
            $status = $this->getReference($this->generateReferenceName(
                UserStatus::class,
                $details['status']
            ));

            $user->setStatus($status);

            $this->addReference($this->generateReferenceName(User::class, $email), $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            UserStatusFixtures::class,
        ];
    }
}
