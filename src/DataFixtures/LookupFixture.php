<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Lookup;
use Doctrine\Persistence\ObjectManager;

abstract class LookupFixture extends AbstractFixture
{
    /**
     * @return string
     */
    abstract protected function getLookupClass(): string;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        try {
            $reflection = new \ReflectionClass($this->getLookupClass());

            $constants = $reflection->getMethod('getHandles')->invoke(null);

            foreach ($constants as $handle) {
                $lookup = $this->createLookup($reflection, $handle);

                $this->addReference($this->generateReferenceName($this->getLookupClass(), $handle), $lookup);

                $manager->persist($lookup);
            }

            $manager->flush();
        } catch (\ReflectionException $e) {
            throw new \LogicException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass
     * @param string $handle
     *
     * @return Lookup
     */
    protected function createLookup(\ReflectionClass $reflectionClass, string $handle): Lookup
    {
        $lookup = $reflectionClass->newInstance($handle, $this->convertHandleToName($handle));

        if (!$lookup instanceof Lookup) {
            throw new \LogicException('Can create only subclasses of Lookup');
        }

        return $lookup;
    }

    /**
     * @param string $handle
     *
     * @return string
     */
    protected function convertHandleToName(string $handle): string
    {
        return ucwords(
            strtolower(
                str_replace('_', ' ', $handle)
            )
        );
    }
}
