<?php

declare(strict_types=1);

namespace App\DTO\Security;

use App\DTO\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ForgotPasswordDTO implements RequestDTOInterface
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(max=180)
     */
    public ?string $email;

    public function __construct(Request $request)
    {
        $data = json_decode((string) $request->getContent(), true);

        $this->email = $data['email'] ?? null;
    }
}
