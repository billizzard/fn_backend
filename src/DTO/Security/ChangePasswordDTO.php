<?php

declare(strict_types=1);

namespace App\DTO\Security;

use App\DTO\RequestDTOInterface;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordDTO implements RequestDTOInterface
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=6, max=30)
     */
    public $oldPassword;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=6, max=30)
     * @AppAssert\Password()
     */
    public $newPassword;

    /**
     * @var string
     *
     * @Assert\Expression("this.isPasswordSame()", message="Passwords do not match")
     */
    public $repeatNewPassword;

    public function __construct(Request $request)
    {
        $data = json_decode((string) $request->getContent(), true);
        $this->oldPassword = $data['oldPassword'];
        $this->newPassword = $data['newPassword'];
        $this->repeatNewPassword = $data['repeatNewPassword'];
    }

    public function isPasswordSame(): bool
    {
        return $this->newPassword === $this->repeatNewPassword;
    }
}
