<?php

declare(strict_types=1);

namespace App\DTO\Security;

use App\DTO\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ConfirmTokenDTO implements RequestDTOInterface
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=48)
     */
    public ?string $token;

    public function __construct(Request $request)
    {
        $this->token = $request->get('token');
    }
}
