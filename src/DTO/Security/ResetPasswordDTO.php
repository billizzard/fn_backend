<?php

declare(strict_types=1);

namespace App\DTO\Security;

use App\DTO\RequestDTOInterface;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordDTO implements RequestDTOInterface
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=6, max=30)
     * @AppAssert\Password()
     */
    public ?string $newPassword;

    /**
     * @var string|null
     *
     * @Assert\Expression("this.isPasswordSame()", message="Passwords do not match")
     */
    public ?string $repeatNewPassword;

    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=48)
     */
    public ?string $token;

    public function __construct(Request $request)
    {
        $data = json_decode((string) $request->getContent(), true);
        $this->newPassword = $data['newPassword'];
        $this->repeatNewPassword = $data['repeatNewPassword'];
        $this->token = $data['token'];
    }

    public function isPasswordSame(): bool
    {
        return $this->newPassword === $this->repeatNewPassword;
    }
}
