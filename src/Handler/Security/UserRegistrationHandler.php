<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\Entity\Security\UserStatus;
use App\Entity\User;
use App\Exception\Security\TooOftenCreationException;
use App\Repository\Security\UserStatusRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegistrationHandler
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var UserStatusRepository
     */
    private UserStatusRepository $statusRepository;

    /**
     * @var EmailConfirmationTokenHandler
     */
    private EmailConfirmationTokenHandler $emailConfirmationTokenHandler;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserStatusRepository $statusRepository
     * @param UserRepository $userRepository
     * @param EmailConfirmationTokenHandler $emailConfirmationTokenHandler
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        UserStatusRepository $statusRepository,
        UserRepository $userRepository,
        EmailConfirmationTokenHandler $emailConfirmationTokenHandler
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->statusRepository = $statusRepository;
        $this->emailConfirmationTokenHandler = $emailConfirmationTokenHandler;
        $this->userRepository = $userRepository;
    }

    public function handlePreEvents(User $user): void
    {
        $this->setPassword($user);
        $this->setStatus($user);
    }

    public function handlePostEvents(User $user): void
    {
        $this->emailConfirmationTokenHandler->createToken($user);
    }

    /**
     * @param User $user
     *
     * @return User|null
     *
     * @throws TooOftenCreationException
     */
    public function handlePreValidateEvents(User $user): ?User
    {
        $existingUser = $this->userRepository->findOneBy(['email' => $user->getEmail()]);

        if ($existingUser !== null && $existingUser->isAwaitingConfirmation()) {
            $this->emailConfirmationTokenHandler->recreateToken($existingUser);

            return $existingUser;
        }

        return null;
    }

    /**
     * @param User $user
     */
    private function setPassword(User $user): void
    {
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $user->getPlainPassword())
        );
    }

    /**
     * @param User $user
     */
    private function setStatus(User $user): void
    {
        $awaitStatus = $this->statusRepository->findOneByHandle(UserStatus::AWAITING_CONFIRMATION);
        if ($awaitStatus === null) {
            throw new \LogicException('Status should exist');
        }

        $user->setStatus($awaitStatus);
    }
}
