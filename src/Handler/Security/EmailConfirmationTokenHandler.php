<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\Entity\Security\EmailConfirmationToken;
use App\Entity\User;
use App\Event\User\EmailConfirmationTokenCreatedEvent;
use App\Exception\Security\TooOftenCreationException;
use App\Repository\Security\EmailConfirmationTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class EmailConfirmationTokenHandler
{
    private EntityManagerInterface $entityManager;

    private EmailConfirmationTokenRepository $tokenRepository;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        EmailConfirmationTokenRepository $tokenRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->entityManager = $entityManager;
        $this->tokenRepository = $tokenRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createToken(User $user): void
    {
        $emailConfirmationToken = new EmailConfirmationToken($user);

        $this->entityManager->persist($emailConfirmationToken);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(new EmailConfirmationTokenCreatedEvent($emailConfirmationToken));
    }

    /**
     * @param User $user
     *
     * @throws TooOftenCreationException
     */
    public function recreateToken(User $user): void
    {
        $existingToken = $this->tokenRepository->findOneBy(['user' => $user->getId()]);

        if ($existingToken !== null) {
            if (!$existingToken->canRecreate()) {
                throw new TooOftenCreationException(sprintf(
                    'Confirmation email can be sent only once event %d minutes',
                    EmailConfirmationToken::RECREATE_MINUTES
                ));
            }

            $this->entityManager->remove($existingToken);
            $this->entityManager->flush();
        }

        $this->createToken($user);
    }
}
