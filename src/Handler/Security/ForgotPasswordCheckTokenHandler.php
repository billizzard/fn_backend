<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\DTO\Security\ForgotPasswordTokenDTO;
use App\Exception\Common\NotFoundApiException;
use App\Exception\Security\TokenExpiredException;
use App\Repository\Security\ForgotPasswordTokenRepository;

class ForgotPasswordCheckTokenHandler
{
    /**
     * @var ForgotPasswordTokenRepository
     */
    private ForgotPasswordTokenRepository $tokenRepository;

    /**
     * @param ForgotPasswordTokenRepository $tokenRepository
     */
    public function __construct(ForgotPasswordTokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @param ForgotPasswordTokenDTO $forgotTokenDTO
     */
    public function handle(ForgotPasswordTokenDTO $forgotTokenDTO): void
    {
        $existToken = $this->tokenRepository->findOneBy(['token' => $forgotTokenDTO->token]);
        if ($existToken === null) {
            throw new NotFoundApiException('Forget token not found');
        }

        if (!$existToken->isValid()) {
            throw new TokenExpiredException('Token expired');
        }
    }
}
