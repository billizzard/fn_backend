<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\DTO\Security\ChangePasswordDTO;
use App\Entity\User;
use App\Exception\Common\ValidationApiException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordHandler
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ChangePasswordDTO $changePasswordDTO
     * @param User $user
     */
    public function handle(ChangePasswordDTO $changePasswordDTO, User $user): void
    {
        if (!$this->isOldPasswordValid($user, $changePasswordDTO->oldPassword)) {
            throw new ValidationApiException('Old password is invalid', 'oldPassword', $changePasswordDTO->oldPassword);
        }

        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $changePasswordDTO->newPassword)
        );

        $this->entityManager->flush();
    }

    private function isOldPasswordValid(User $user, string $password): bool
    {
        return $this->passwordEncoder->isPasswordValid($user, $password);
    }
}
