<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\DTO\Security\ForgotPasswordDTO;
use App\Entity\Security\ForgotPasswordToken;
use App\Entity\User;
use App\Event\User\ForgotPasswordTokenCreatedEvent;
use App\Exception\Common\NotFoundApiException;
use App\Exception\Security\TooOftenCreationException;
use App\Repository\Security\ForgotPasswordTokenRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ForgotPasswordHandler
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var ForgotPasswordTokenRepository
     */
    private ForgotPasswordTokenRepository $tokenRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param UserRepository $userRepository
     * @param ForgotPasswordTokenRepository $tokenRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        UserRepository $userRepository,
        ForgotPasswordTokenRepository $tokenRepository
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @param ForgotPasswordDTO $forgotPasswordDTO
     */
    public function handle(ForgotPasswordDTO $forgotPasswordDTO): void
    {
        $user = $this->userRepository->findOneBy(['email' => $forgotPasswordDTO->email]);
        if ($user === null) {
            throw new NotFoundApiException();
        }

        $existToken = $this->tokenRepository->findOneBy(['user' => $user->getId()]);
        if ($existToken !== null) {
            if (!$existToken->canRecreate()) {
                throw new TooOftenCreationException(sprintf(
                    'Restore password request can be sent only once in %d minutes',
                    ForgotPasswordToken::RECREATE_MINUTES
                ));
            }

            $forgotToken = $this->recreateToken($user, $existToken);
        } else {
            $forgotToken = $this->createToken($user);
        }

        $this->eventDispatcher->dispatch(new ForgotPasswordTokenCreatedEvent($forgotToken));
    }

    private function createToken(User $user): ForgotPasswordToken
    {
        $forgotToken = new ForgotPasswordToken($user);
        $this->entityManager->persist($forgotToken);
        $this->entityManager->flush();

        return $forgotToken;
    }

    private function recreateToken(User $user, ForgotPasswordToken $token): ForgotPasswordToken
    {
        $this->entityManager->remove($token);
        $this->entityManager->flush();

        return $this->createToken($user);
    }
}
