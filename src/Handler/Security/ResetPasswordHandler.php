<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\DTO\Security\ResetPasswordDTO;
use App\Exception\Common\NotFoundApiException;
use App\Exception\Security\TokenExpiredException;
use App\Repository\Security\ForgotPasswordTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordHandler
{
    /**
     * @var ForgotPasswordTokenRepository
     */
    private ForgotPasswordTokenRepository $tokenRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @param ForgotPasswordTokenRepository $tokenRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ForgotPasswordTokenRepository $tokenRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ) {
        $this->tokenRepository = $tokenRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ResetPasswordDTO $resetPasswordDTO
     */
    public function handle(ResetPasswordDTO $resetPasswordDTO): void
    {
        $existToken = $this->tokenRepository->findOneBy(['token' => $resetPasswordDTO->token]);
        if ($existToken === null) {
            throw new NotFoundApiException('Forget token not found');
        }

        if (!$existToken->isValid()) {
            throw new TokenExpiredException('Token expired');
        }

        $user = $existToken->getUser();
        if ($resetPasswordDTO->newPassword === null) {
            throw new \LogicException('Can not be null');
        }

        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $resetPasswordDTO->newPassword)
        );

        $this->entityManager->remove($existToken);
        $this->entityManager->flush();
    }
}
