<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\DTO\Security\ConfirmTokenDTO;
use App\Entity\Security\UserStatus;
use App\Exception\Common\NotFoundApiException;
use App\Repository\Security\EmailConfirmationTokenRepository;
use App\Repository\Security\UserStatusRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserConfirmationHandler
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var EmailConfirmationTokenRepository
     */
    private EmailConfirmationTokenRepository $tokenRepository;

    /**
     * @var UserStatusRepository
     */
    private UserStatusRepository $userStatusRepository;

    /**
     * @param EmailConfirmationTokenRepository $tokenRepository
     * @param UserStatusRepository $userStatusRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EmailConfirmationTokenRepository $tokenRepository,
        UserStatusRepository $userStatusRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->tokenRepository = $tokenRepository;
        $this->userStatusRepository = $userStatusRepository;
    }

    /**
     * @param ConfirmTokenDTO $confirmEmailDTO
     */
    public function handle(ConfirmTokenDTO $confirmEmailDTO): void
    {
        $emailConfirmationToken = $this->tokenRepository->findOneBy(['token' => $confirmEmailDTO->token]);
        if ($emailConfirmationToken === null) {
            throw new NotFoundApiException('Confirmation token not found');
        }

        $status = $this->userStatusRepository->findOneByHandle(UserStatus::ACTIVE);
        if ($status === null) {
            throw new \LogicException('Status should exist');
        }

        $emailConfirmationToken->getUser()->setStatus($status);

        $this->entityManager->remove($emailConfirmationToken);
        $this->entityManager->flush();
    }
}
