<?php

declare(strict_types=1);

namespace App\Swagger;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class MergeDocumentationSwagger implements NormalizerInterface
{
    /**
     * @var NormalizerInterface
     */
    private $decorated;

    /**
     * @var iterable
     */
    private iterable $swaggerDocs;

    /**
     * @param NormalizerInterface $decorated
     * @param \Traversable|DocumentationSwaggerInterface[] $swaggerDocs
     */
    public function __construct(NormalizerInterface $decorated, iterable $swaggerDocs)
    {
        $this->decorated = $decorated;
        $this->swaggerDocs = iterator_to_array($swaggerDocs);
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        /** @var DocumentationSwaggerInterface $swaggerDoc */
        foreach ($this->swaggerDocs as $swaggerDoc) {
            $docs['paths'][$swaggerDoc->getPath()][$swaggerDoc->getMethod()] = [
                'tags' => $swaggerDoc->getNames(),
                'parameters' => $swaggerDoc->getParameters(),
                'responses' => $swaggerDoc->getResponses(),
                'security' => $swaggerDoc->hasSecurity() ? $this->getSecurityParams() : [],
            ];
        }

        return $docs;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    private function getSecurityParams(): array
    {
        return [
            'type' => 'http',
            'scheme' => 'bearer',
            'bearerFormat' => 'JWT',
        ];
    }
}
