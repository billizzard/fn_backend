<?php

declare(strict_types=1);

namespace App\Swagger;

interface DocumentationSwaggerInterface
{
    public function getResponses(): array;

    public function getNames(): array;

    public function getPath(): string;

    public function getMethod(): string;

    public function getParameters(): array;

    public function hasSecurity(): bool;
}
