<?php

declare(strict_types=1);

namespace App\Swagger\Security;

use App\Swagger\DocumentationSwaggerInterface;

class ConfirmEmailSwagger implements DocumentationSwaggerInterface
{
    public function getResponses(): array
    {
        return [
            '200' => [
                'description' => 'Email confirmed successfully',
            ],
            '404' => [
                'description' => 'Token not found',
            ],
        ];
    }

    public function getNames(): array
    {
        return ['Security'];
    }

    public function getPath(): string
    {
        return '/api/security/confirm-email/{token}';
    }

    public function getMethod(): string
    {
        return 'get';
    }

    public function hasSecurity(): bool
    {
        return false;
    }

    public function getParameters(): array
    {
        return [
            [
                'name' => 'token',
                'description' => 'Confirmation token',
                'in' => 'path',
            ],
        ];
    }
}
