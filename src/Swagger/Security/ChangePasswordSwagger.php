<?php

declare(strict_types=1);

namespace App\Swagger\Security;

use App\Swagger\DocumentationSwaggerInterface;

class ChangePasswordSwagger implements DocumentationSwaggerInterface
{
    public function getResponses(): array
    {
        return [
            '200' => [
                'description' => 'Password reset successfully',
            ],
            '400' => [
                'description' => 'Validation errors',
            ],
        ];
    }

    public function getNames(): array
    {
        return ['Security'];
    }

    public function getPath(): string
    {
        return '/api/security/change-password';
    }

    public function getMethod(): string
    {
        return 'post';
    }

    public function getParameters(): array
    {
        return [
            [
                'name' => 'oldPassword',
                'description' => 'Old password',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
            [
                'name' => 'newPassword',
                'description' => 'New password',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
            [
                'name' => 'repeatNewPassword',
                'description' => 'Repeat new password',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
        ];
    }

    public function hasSecurity(): bool
    {
        return true;
    }
}
