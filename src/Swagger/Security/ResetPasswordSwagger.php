<?php

declare(strict_types=1);

namespace App\Swagger\Security;

use App\Swagger\DocumentationSwaggerInterface;

class ResetPasswordSwagger implements DocumentationSwaggerInterface
{
    public function getResponses(): array
    {
        return [
            '200' => [
                'description' => 'Password reset successfully',
            ],
            '400' => [
                'description' => 'Validation errors',
            ],
            '404' => [
                'description' => 'Token not found',
            ],
        ];
    }

    public function getNames(): array
    {
        return ['Security'];
    }

    public function getPath(): string
    {
        return '/api/security/reset-password';
    }

    public function getMethod(): string
    {
        return 'post';
    }

    public function hasSecurity(): bool
    {
        return false;
    }

    public function getParameters(): array
    {
        return [
            [
                'name' => 'newPassword',
                'description' => 'New password',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
            [
                'name' => 'repeatNewPassword',
                'description' => 'Repeat new password',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
            [
                'name' => 'token',
                'description' => 'Forgot password token',
                'in' => 'body',
                'required' => true,
                'type' => 'string',
            ],
        ];
    }
}
