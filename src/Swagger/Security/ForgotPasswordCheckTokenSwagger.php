<?php

declare(strict_types=1);

namespace App\Swagger\Security;

use App\Swagger\DocumentationSwaggerInterface;

class ForgotPasswordCheckTokenSwagger implements DocumentationSwaggerInterface
{
    public function getResponses(): array
    {
        return [
            '200' => [
                'description' => 'Token is valid',
            ],
            '404' => [
                'description' => 'Token not exists',
            ],
            '400' => [
                'description' => 'Token expired',
            ],
        ];
    }

    public function getNames(): array
    {
        return ['Security'];
    }

    public function getPath(): string
    {
        return '/api/security/forgot-password/check/{token}';
    }

    public function getMethod(): string
    {
        return 'get';
    }

    public function hasSecurity(): bool
    {
        return false;
    }

    public function getParameters(): array
    {
        return [
            [
                'summary' => 'Check is token valid',
                'name' => 'token',
                'description' => 'Forgot password token',
                'in' => 'path',
            ],
        ];
    }
}
