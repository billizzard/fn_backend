<?php

declare(strict_types=1);

namespace App\Swagger\Security;

use App\Swagger\DocumentationSwaggerInterface;

class ForgotPasswordSwagger implements DocumentationSwaggerInterface
{
    public function getResponses(): array
    {
        return [
            '200' => [
                'description' => 'Token successfully created',
            ],
            '403' => [
                'description' => 'Reset password more than once every 5 min',
            ],
        ];
    }

    public function getNames(): array
    {
        return ['Security'];
    }

    public function getPath(): string
    {
        return '/api/security/forgot-password';
    }

    public function getMethod(): string
    {
        return 'post';
    }

    public function hasSecurity(): bool
    {
        return false;
    }

    public function getParameters(): array
    {
        return [
            [
                'name' => 'email',
                'description' => 'Email',
                'in' => 'body',
                'type' => 'string',
                'required' => true,
            ],
        ];
    }
}
