<?php

declare(strict_types=1);

namespace App\Repository\Security;

use App\Entity\Security\ForgotPasswordToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ForgotPasswordToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForgotPasswordToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForgotPasswordToken[]    findAll()
 * @method ForgotPasswordToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForgotPasswordTokenRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForgotPasswordToken::class);
    }
}
