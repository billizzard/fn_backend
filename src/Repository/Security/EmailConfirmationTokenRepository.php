<?php

declare(strict_types=1);

namespace App\Repository\Security;

use App\Entity\Security\EmailConfirmationToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EmailConfirmationToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailConfirmationToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailConfirmationToken[]    findAll()
 * @method EmailConfirmationToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailConfirmationTokenRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailConfirmationToken::class);
    }
}
