<?php

declare(strict_types=1);

namespace App\Exception\Security;

use App\Exception\ApiExceptionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TooOftenCreationException extends AccessDeniedHttpException implements ApiExceptionInterface
{
    public function getErrorCode(): string
    {
        return 's-100';
    }
}
