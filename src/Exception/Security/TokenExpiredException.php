<?php

declare(strict_types=1);

namespace App\Exception\Security;

use App\Exception\ApiExceptionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TokenExpiredException extends BadRequestHttpException implements ApiExceptionInterface
{
    public function getErrorCode(): string
    {
        return 's-101';
    }
}
