<?php

declare(strict_types=1);

namespace App\Exception\Common;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\Exception\ValidationException as BaseValidationException;
use App\Exception\ApiExceptionInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ValidationApiException extends BaseValidationException implements ApiExceptionInterface
{
    /**
     * ValidationApiException constructor.
     *
     * @param string $message
     * @param string $path
     * @param mixed $value
     */
    public function __construct(string $message, string $path, $value)
    {
        parent::__construct($message);

        throw new ValidationException(
            new ConstraintViolationList([
                new ConstraintViolation(
                    $message,
                    null,
                    [],
                    $value,
                    $path,
                    $value
                ),
            ])
        );
    }

    public function getErrorCode(): string
    {
        return 'c-400';
    }
}
