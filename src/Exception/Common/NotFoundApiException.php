<?php

declare(strict_types=1);

namespace App\Exception\Common;

use App\Exception\ApiExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotFoundApiException extends NotFoundHttpException implements ApiExceptionInterface
{
    public function getErrorCode(): string
    {
        return 'c-404';
    }
}
