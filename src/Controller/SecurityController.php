<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\Security\ChangePasswordDTO;
use App\DTO\Security\ConfirmTokenDTO;
use App\DTO\Security\ForgotPasswordDTO;
use App\DTO\Security\ForgotPasswordTokenDTO;
use App\DTO\Security\ResetPasswordDTO;
use App\Entity\User;
use App\Handler\Security\ChangePasswordHandler;
use App\Handler\Security\ForgotPasswordCheckTokenHandler;
use App\Handler\Security\ForgotPasswordHandler;
use App\Handler\Security\ResetPasswordHandler;
use App\Handler\Security\UserConfirmationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/security", name="security.")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/confirm-email/{token}", name="confirm_email", methods={"GET"})
     */
    public function emailConfirmation(ConfirmTokenDTO $confirmTokenDTO, UserConfirmationHandler $handler, string $token): JsonResponse
    {
        $handler->handle($confirmTokenDTO);

        return new JsonResponse(null);
    }

    /**
     * @Route("/forgot-password", name="forgot_password", methods={"POST"})
     */
    public function forgotPassword(ForgotPasswordDTO $forgotPasswordDTO, ForgotPasswordHandler $handler): JsonResponse
    {
        $handler->handle($forgotPasswordDTO);

        return new JsonResponse(null);
    }

    /**
     * @Route("/forgot-password/check/{token}", name="forgot_password.check", methods={"GET"})
     */
    public function forgotPasswordCheckToken(ForgotPasswordTokenDTO $forgotPasswordTokenDTO, ForgotPasswordCheckTokenHandler $handler): JsonResponse
    {
        $handler->handle($forgotPasswordTokenDTO);

        return new JsonResponse(null);
    }

    /**
     * @Route("/reset-password", name="reset_password", methods={"POST"})
     */
    public function resetPassword(ResetPasswordDTO $resetPasswordDTO, ResetPasswordHandler $handler): JsonResponse
    {
        $handler->handle($resetPasswordDTO);

        return new JsonResponse(null);
    }

    /**
     * @Route("/change-password", name="change_password", methods={"POST"})
     */
    public function changePassword(ChangePasswordDTO $changePasswordDTO, ChangePasswordHandler $handler): JsonResponse
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Action only for user');
        }

        $handler->handle($changePasswordDTO, $user);

        return new JsonResponse(null);
    }
}
