<?php

declare(strict_types=1);

namespace App\Service\Email;

use App\Email\EmailInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

class MailerService
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @var Environment
     */
    private Environment $twig;

    public function __construct(
        MailerInterface $mailer,
        Environment $twig
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @param EmailInterface $email
     *
     * @throws TransportExceptionInterface
     */
    public function send(EmailInterface $email): void
    {
        $email->initialize($this->twig);

        $this->mailer->send($email->getCompiledEmail());
    }
}
