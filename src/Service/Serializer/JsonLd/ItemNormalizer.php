<?php

declare(strict_types=1);

namespace App\Service\Serializer\JsonLd;

use ApiPlatform\Core\JsonLd\Serializer\ItemNormalizer as Normalizer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ItemNormalizer
{
    /**
     * @var Normalizer
     */
    private Normalizer $normalizer;

    public function __construct(Normalizer $itemNormalizer)
    {
        $this->normalizer = $itemNormalizer;
    }

    /**
     * @param mixed $object
     *
     * @return mixed
     *
     * @throws ExceptionInterface
     */
    public function normalize($object)
    {
        return $this->normalizer->normalize($object);
    }
}
