<?php

declare(strict_types=1);

namespace App\Email;

use Symfony\Component\Mime\Email;
use Twig\Environment;

/**
 * Basic implementation of EmailInterface.
 */
abstract class AbstractEmail implements EmailInterface
{
    /**
     * @var Environment|null
     */
    private $twig;

    /**
     * @var string|null
     */
    private $from;

    /**
     * @var string|null
     */
    private $to;

    /**
     * @var bool
     */
    private $initialized = false;

    public function initialize(Environment $twig): void
    {
        $this->twig = $twig;
        $this->initialized = true;
    }

    public function getCompiledEmail(): Email
    {
        $email = (new Email())->subject($this->getSubject());

        if (!$this->initialized) {
            throw new \LogicException('Message should be initialized before compilation');
        }

        $from = $this->getFrom();
        if (null !== $from) {
            $email->from($from);
        }

        $to = $this->getTo();
        if (null !== $to) {
            $email->to($to);
        }

        $email->text($this->getTextBody());
        $email->html($this->getHtmlBody());

        return $email;
    }

    /**
     * @return string
     */
    abstract protected function getSubject(): string;

    /**
     * @return string
     */
    abstract protected function getHtmlBody(): string;

    /**
     * @return string
     */
    abstract protected function getTextBody(): string;

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @param string $email
     *
     * @return AbstractEmail
     */
    public function setFrom(string $email): self
    {
        $this->from = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param string $recipient
     *
     * @return AbstractEmail
     */
    public function setTo(string $recipient): self
    {
        $this->to = $recipient;

        return $this;
    }

    /**
     * @param string $name
     * @param array $parameters
     *
     * @return string
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(string $name, array $parameters = []): string
    {
        if (null === $this->twig || !$this->initialized) {
            throw new \LogicException('Message should be initialized before render');
        }

        return $this->twig->render($name, $parameters);
    }
}
