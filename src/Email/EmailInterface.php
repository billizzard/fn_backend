<?php

declare(strict_types=1);

namespace App\Email;

use Symfony\Component\Mime\Email;
use Twig\Environment;

/**
 * Email classes should implement this interface to be used by MailerService.
 */
interface EmailInterface
{
    public function initialize(Environment $twig): void;

    public function getCompiledEmail(): Email;

    public function getTo(): ?string;

    public function setTo(string $recipient): self;

    public function getFrom(): ?string;

    public function setFrom(string $recipient): self;
}
