<?php

declare(strict_types=1);

namespace App\Email\Customer;

use App\Email\AbstractEmail;
use App\Entity\Security\ForgotPasswordToken;

/**
 * Email that is sent to customer to confirm their email.
 */
class ForgotPasswordEmail extends AbstractEmail
{
    /**
     * @var ForgotPasswordToken
     */
    private $forgotPasswordToken;

    /**
     * @param ForgotPasswordToken $forgotPasswordToken
     */
    public function __construct(ForgotPasswordToken $forgotPasswordToken)
    {
        $this->forgotPasswordToken = $forgotPasswordToken;
    }

    /**
     * @return string
     */
    protected function getSubject(): string
    {
        return 'Reset your password';
    }

    protected function getHtmlBody(): string
    {
        return $this->render('email/user/forgot_password_email.html.twig', [
            'token' => $this->forgotPasswordToken->getToken(),
        ]);
    }

    protected function getTextBody(): string
    {
        return $this->render('email/user/forgot_password_email.txt.twig', [
            'token' => $this->forgotPasswordToken->getToken(),
        ]);
    }
}
