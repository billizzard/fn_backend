<?php

declare(strict_types=1);

namespace App\Email\Customer;

use App\Email\AbstractEmail;
use App\Entity\Security\EmailConfirmationToken;

/**
 * Email that is sent to customer to confirm their email.
 */
class EmailConfirmationEmail extends AbstractEmail
{
    /**
     * @var EmailConfirmationToken
     */
    private $emailConfirmationToken;

    /**
     * @param EmailConfirmationToken $emailConfirmationToken
     */
    public function __construct(EmailConfirmationToken $emailConfirmationToken)
    {
        $this->emailConfirmationToken = $emailConfirmationToken;
    }

    /**
     * @return string
     */
    protected function getSubject(): string
    {
        return 'Confirm Your Email';
    }

    /**
     * @return string
     */
    protected function getHtmlBody(): string
    {
        return $this->render('email/user/confirm_email.html.twig', [
            'token' => $this->emailConfirmationToken->getToken(),
        ]);
    }

    protected function getTextBody(): string
    {
        return $this->render('email/user/confirm_email.txt.twig', [
            'token' => $this->emailConfirmationToken->getToken(),
        ]);
    }
}
