<?php

declare(strict_types=1);

namespace App\Entity\Security;

use App\Entity\Lookup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Security\UserStatusRepository")
 */
class UserStatus extends Lookup
{
    public const ACTIVE = 'ACTIVE';
    public const AWAITING_CONFIRMATION = 'AWAITING_CONFIRMATION';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $handle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25)
     */
    protected $name;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
