<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class Lookup
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     */
    protected $handle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @param string $handle
     * @param string $name
     */
    public function __construct(string $handle, string $name)
    {
        $this->handle = $handle;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $handle
     *
     * @return bool
     */
    public static function isValid(string $handle): bool
    {
        return in_array($handle, self::getHandles(), true);
    }

    /**
     * @param string $handle
     *
     * @return bool
     */
    public function equals(string $handle): bool
    {
        return $this->handle === $handle;
    }

    /**
     * @return string[]
     */
    public static function getHandles(): array
    {
        try {
            $reflection = new \ReflectionClass(static::class);

            return $reflection->getConstants();
        } catch (\ReflectionException $e) {
            return [];
        }
    }
}
