<?php

declare(strict_types=1);

namespace App\Entity;

use App\Helper\DateHelper;
use App\Helper\TokenGenerator;
use Doctrine\ORM\Mapping as ORM;

abstract class Token
{
    public const RECREATE_MINUTES = 5;
    protected const LIFE_TIME_MINUTES = 30;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=48)
     */
    protected $token;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="datetime_immutable")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->token = TokenGenerator::generateUuid4();

        try {
            $this->createdAt = new \DateTimeImmutable();
        } catch (\Exception $e) {
            throw new \RuntimeException('Unable to create DateTime', $e->getCode(), $e);
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return DateHelper::getMinutesSinceDate($this->getCreatedAt()) < static::LIFE_TIME_MINUTES;
    }

    /**
     * @return bool
     */
    public function canRecreate(): bool
    {
        return DateHelper::getMinutesSinceDate($this->getCreatedAt()) > static::RECREATE_MINUTES;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
