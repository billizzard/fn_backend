<?php

declare(strict_types=1);

namespace App\Helper;

/**
 * Add methods for work with date.
 */
class DateHelper
{
    private const SECONDS_IN_MINUTE = 60;

    /**
     * @return \DateTimeImmutable
     */
    public static function now(): \DateTimeImmutable
    {
        try {
            return new \DateTimeImmutable('now');
        } catch (\Exception $e) {
            throw new \RuntimeException('Unable to create DateTime', $e->getCode(), $e);
        }
    }

    /**
     * @param int $timestamp
     *
     * @return \DateTimeImmutable
     */
    public static function fromUnix(int $timestamp): \DateTimeImmutable
    {
        $datetime = \DateTimeImmutable::createFromFormat('U', (string) $timestamp);

        if ($datetime === false) {
            throw new \RuntimeException('Unable to create DateTime');
        }

        return $datetime;
    }

    /**
     * @param \DateTimeInterface $date
     *
     * @return int
     */
    public static function getMinutesSinceDate(\DateTimeInterface $date): int
    {
        return (int) floor(abs(self::now()->getTimestamp() - $date->getTimestamp()) / self::SECONDS_IN_MINUTE);
    }
}
