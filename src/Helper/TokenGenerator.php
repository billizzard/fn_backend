<?php

declare(strict_types=1);

namespace App\Helper;

/**
 * Generates tokens.
 */
class TokenGenerator
{
    /**
     * @return string
     */
    public static function generateUuid4(): string
    {
        return uuid_create(UUID_TYPE_RANDOM);
    }
}
