<?php

declare(strict_types=1);

namespace App\EventSubscriber\Email;

use App\Email\Customer\ForgotPasswordEmail;
use App\Email\EmailInterface;
use App\Event\User\ForgotPasswordTokenCreatedEvent;

class ForgotPasswordCreationSubscriber extends AbstractEmailSubscriber
{
    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @param string $systemEmail
     *
     * @required
     */
    public function setFromEmail(string $systemEmail): void
    {
        $this->fromEmail = $systemEmail;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ForgotPasswordTokenCreatedEvent::class => 'sendForgotPasswordEmail',
        ];
    }

    public function sendForgotPasswordEmail(ForgotPasswordTokenCreatedEvent $event): void
    {
        $this->sendEmail($this->createConfirmationEmail($event));
    }

    private function createConfirmationEmail(ForgotPasswordTokenCreatedEvent $event): EmailInterface
    {
        $forgotPasswordToken = $event->getToken();

        $email = new ForgotPasswordEmail($forgotPasswordToken);
        $email->setFrom($this->fromEmail);
        $email->setTo($forgotPasswordToken->getUser()->getEmail());

        return $email;
    }
}
