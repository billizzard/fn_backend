<?php

declare(strict_types=1);

namespace App\EventSubscriber\Email;

use App\Email\EmailInterface;
use App\Service\Email\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

abstract class AbstractEmailSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerService
     */
    private MailerService $mailerService;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $emailSendingLogger, MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
        $this->logger = $emailSendingLogger;
    }

    public function sendEmail(EmailInterface $email): void
    {
        try {
            $this->mailerService->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error('Can not send confirmation email', [
                'email' => $email->getTo(),
            ]);
        }
    }
}
