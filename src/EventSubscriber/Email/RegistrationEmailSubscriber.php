<?php

declare(strict_types=1);

namespace App\EventSubscriber\Email;

use App\Email\Customer\EmailConfirmationEmail;
use App\Email\EmailInterface;
use App\Event\User\EmailConfirmationTokenCreatedEvent;

class RegistrationEmailSubscriber extends AbstractEmailSubscriber
{
    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @param string $systemEmail
     *
     * @required
     */
    public function setFromEmail(string $systemEmail): void
    {
        $this->fromEmail = $systemEmail;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EmailConfirmationTokenCreatedEvent::class => 'sendConfirmationEmail',
        ];
    }

    public function sendConfirmationEmail(EmailConfirmationTokenCreatedEvent $event): void
    {
        $this->sendEmail($this->createConfirmationEmail($event));
    }

    private function createConfirmationEmail(EmailConfirmationTokenCreatedEvent $event): EmailInterface
    {
        $confirmationToken = $event->getToken();

        $email = new EmailConfirmationEmail($confirmationToken);
        $email->setFrom($this->fromEmail);
        $email->setTo($confirmationToken->getUser()->getEmail());

        return $email;
    }
}
