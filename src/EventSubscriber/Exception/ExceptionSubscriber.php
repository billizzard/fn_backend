<?php

declare(strict_types=1);

namespace App\EventSubscriber\Exception;

use App\Exception\ApiExceptionInterface;
use App\Service\Serializer\JsonLd\ItemNormalizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var ItemNormalizer
     */
    private ItemNormalizer $itemNormalizer;

    public function __construct(ItemNormalizer $itemNormalizer)
    {
        $this->itemNormalizer = $itemNormalizer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 0],
            ],
        ];
    }

    public function processException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $error_code = $exception->getCode();
        if ($exception instanceof ApiExceptionInterface) {
            $error_code = $exception->getErrorCode();
        }

        $event->setResponse(new JsonResponse([
            'error_code' => $error_code,
            'message' => $exception->getMessage(),
        ]));
    }
}
