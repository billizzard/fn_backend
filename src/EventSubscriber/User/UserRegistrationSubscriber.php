<?php

declare(strict_types=1);

namespace App\EventSubscriber\User;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Exception\Security\TooOftenCreationException;
use App\Handler\Security\UserRegistrationHandler;
use App\Repository\UserRepository;
use App\Service\Serializer\JsonLd\ItemNormalizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UserRegistrationSubscriber implements EventSubscriberInterface
{
    private UserRegistrationHandler $userRegistrationHandler;

    private UserRepository $userRepository;

    private ItemNormalizer $apiNormalizer;

    public function __construct(UserRegistrationHandler $userRegistrationHandler, ItemNormalizer $apiNormalizer, UserRepository $userRepository)
    {
        $this->userRegistrationHandler = $userRegistrationHandler;
        $this->userRepository = $userRepository;
        $this->apiNormalizer = $apiNormalizer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['preUserValidate', EventPriorities::PRE_VALIDATE],
                ['preUserRegistered', EventPriorities::PRE_WRITE],
                ['postUserRegistered', EventPriorities::POST_WRITE],
            ],
        ];
    }

    /**
     * @param ViewEvent $event
     *
     * @throws TooOftenCreationException
     */
    public function preUserValidate(ViewEvent $event): void
    {
        $entity = $event->getControllerResult();
        if (!$entity instanceof User) {
            return;
        }

        $method = $event->getRequest()->getMethod();
        if ($method !== Request::METHOD_POST) {
            return;
        }

        $existingUser = $this->userRegistrationHandler->handlePreValidateEvents($entity);

        if ($existingUser !== null) {
            try {
                $event->setResponse(new JsonResponse($this->apiNormalizer->normalize($existingUser), JsonResponse::HTTP_OK));
            } catch (ExceptionInterface $e) {
                throw new \LogicException('User should be serializable');
            }
        }
    }

    public function preUserRegistered(ViewEvent $event): void
    {
        $entity = $event->getControllerResult();
        if (!$entity instanceof User) {
            return;
        }

        $method = $event->getRequest()->getMethod();
        if ($method !== Request::METHOD_POST) {
            return;
        }

        $this->userRegistrationHandler->handlePreEvents($entity);
    }

    public function postUserRegistered(ViewEvent $event): void
    {
        $entity = $event->getControllerResult();
        if (!$entity instanceof User) {
            return;
        }

        $method = $event->getRequest()->getMethod();
        if ($method !== Request::METHOD_POST) {
            return;
        }

        $this->userRegistrationHandler->handlePostEvents($entity);
    }
}
