# README #

### Start dev environment ###

#### Run docker-compose

```bash
docker-compose -f docker-compose.local.yml up
```

#### Create jwt keys

```bash
mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

#### Create jwt keys for test environment

```bash
openssl genrsa -out config/jwt/private-test.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private-test.pem -out config/jwt/public-test.pem
```

#### Create database structure

```bash
docker-compose -f docker-compose.local.yml exec php composer install
docker-compose -f docker-compose.local.yml exec php php bin/console d:d:d --force
docker-compose -f docker-compose.local.yml exec php php bin/console d:d:c
docker-compose -f docker-compose.local.yml exec php php bin/console d:m:m -n
docker-compose -f docker-compose.local.yml exec php php bin/console d:f:l -n
```

#### Work with repository
Before push need to run command for checking code:
```bash
docker-compose -f docker-compose.local.yml exec php composer analyze
docker-compose -f docker-compose.local.yml exec php composer t:d:r
docker-compose -f docker-compose.local.yml exec php php bin/phpunit tests/
```

### Start prod environment ###

#### Build frontend image

.... in ci
