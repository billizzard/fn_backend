<?php

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BaseApi extends ApiTestCase
{
    /**
     * @param string $email
     * @param string $password
     *
     * @return Client|null
     */
    protected function createAuthenticatedClient($email = 'auth_user1@example.com', $password = 'q1w2e3Q!W@E#'): ?Client
    {
        $client = static::createClient();
        try {
            $response = $client->request('POST', '/api/login', [
                'json' => [
                    'email' => $email,
                    'password' => $password,
                ]
            ]);

            $data = json_decode($response->getContent(), true);
        } catch (ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            return null;
        }

        $client = static::createClient();
        $client->setDefaultOptions(['auth_bearer' => $data['token']]);

        return $client;
    }
}
