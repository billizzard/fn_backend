<?php

namespace App\Tests\Functional\User;

use App\Tests\Functional\BaseApi;

class LoginTest extends BaseApi
{
    public function testSuccessLogin(): void
    {
        $client = $this->createAuthenticatedClient();

        $this->assertNotNull($client);
    }

    public function testNotExistsUser(): void
    {
        $response = $this->createAuthenticatedClient('not_existing_user@example.com', 'q1w2e3Q!W@E#');
        
        $this->assertNull($response);
    }

    public function testAwaitingUser(): void
    {
        $this->createAuthenticatedClient('await_user1@example.com', 'q1w2e3Q!W@E#');

        $this->assertResponseStatusCodeSame(401);
    }
}
