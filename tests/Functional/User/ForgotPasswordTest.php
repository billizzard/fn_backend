<?php

namespace App\Tests\Functional\User;

use App\Entity\Security\ForgotPasswordToken;
use App\Entity\User;
use App\Tests\Functional\BaseApi;
use App\Tests\Functional\DatabaseTrait;

class ForgotPasswordTest extends BaseApi
{
    use DatabaseTrait;

    public function testForgotTokenCreateSuccess(): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/security/forgot-password', ['json' => [
            'email' => 'auth_user1@example.com',
        ]]);

        $this->assertResponseIsSuccessful();

        $user = $this->getRepositoryResult(User::class, ['email' => 'auth_user1@example.com']);
        /** @var ForgotPasswordToken $token */
        $token = $this->getRepositoryResult(ForgotPasswordToken::class, ['user' => $user->getId()]);

        $this->assertNotNull($token);
    }

    public function testDoubleForgotToken(): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/security/forgot-password', ['json' => [
            'email' => 'auth_user1@example.com',
        ]]);

        $this->assertResponseIsSuccessful();

        $client->request('POST', '/api/security/forgot-password', ['json' => [
            'email' => 'auth_user1@example.com',
        ]]);

        $this->assertResponseStatusCodeSame(403);
    }
}
