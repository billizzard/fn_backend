<?php

namespace App\Tests\Functional\User;

use App\DataFixtures\User\ForgotPasswordTokenFixtures;
use App\Entity\Security\ForgotPasswordToken;
use App\Tests\Functional\BaseApi;
use App\Tests\Functional\DatabaseTrait;

class ResetPasswordTest extends BaseApi
{
    use DatabaseTrait;

    public function testResetPasswordSuccess(): void
    {
        $authClient = $this->createAuthenticatedClient('forgot_password_user@example.com', 'q1w2e3Q!W@E#');
        $this->assertNotNull($authClient);

        /** @var ForgotPasswordToken $token */
        $token = $this->getRepositoryResult(ForgotPasswordToken::class, ['token' => ForgotPasswordTokenFixtures::TOKENS[0]['token']]);

        $client = static::createClient();
        $client->request('POST', '/api/security/reset-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#111',
            'repeatNewPassword' => 'q1w2e3Q!W@E#111',
            'token' => $token->getToken(),
        ]]);

        $this->assertResponseIsSuccessful();

        $authClient = $this->createAuthenticatedClient('forgot_password_user@example.com', 'q1w2e3Q!W@E#');
        $this->assertNull($authClient);

        $authClient = $this->createAuthenticatedClient('forgot_password_user@example.com', 'q1w2e3Q!W@E#111');
        $this->assertNotNull($authClient);
    }

    public function testNotSamePassword(): void
    {
        $client = static::createClient();

        /** @var ForgotPasswordToken $token */
        $token = $this->getRepositoryResult(ForgotPasswordToken::class, ['token' => ForgotPasswordTokenFixtures::TOKENS[0]['token']]);

        $client->request('POST', '/api/security/reset-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#',
            'repeatNewPassword' => 'q1w2e3Q!W@E#111',
            'token' => $token->getToken(),
        ]]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testTokenNotFound(): void
    {
        $client = static::createClient();

        $client->request('POST', '/api/security/reset-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#',
            'repeatNewPassword' => 'q1w2e3Q!W@E#',
            'token' => 'not_existing_token',
        ]]);

        $this->assertResponseStatusCodeSame(404);
    }
}
