<?php

namespace App\Tests\Functional\User;

use App\Tests\Functional\BaseApi;

class ChangePasswordTest extends BaseApi
{
    public function testChangePasswordSuccess(): void
    {
        $authClient = $this->createAuthenticatedClient('auth_user1@example.com', 'q1w2e3Q!W@E#');
        $this->assertNotNull($authClient);

        $authClient->request('POST', '/api/security/change-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#111',
            'repeatNewPassword' => 'q1w2e3Q!W@E#111',
            'oldPassword' => 'q1w2e3Q!W@E#',
        ]]);

        $this->assertResponseIsSuccessful();
    }

    public function testOldPasswordWrong(): void
    {
        $authClient = $this->createAuthenticatedClient('auth_user1@example.com', 'q1w2e3Q!W@E#');
        $this->assertNotNull($authClient);

        $authClient->request('POST', '/api/security/change-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#111',
            'repeatNewPassword' => 'q1w2e3Q!W@E#111',
            'oldPassword' => 'wrong_password',
        ]]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testNotStrongPassword(): void
    {
        $authClient = $this->createAuthenticatedClient('auth_user1@example.com', 'q1w2e3Q!W@E#');
        $this->assertNotNull($authClient);

        $authClient->request('POST', '/api/security/change-password', ['json' => [
            'newPassword' => 'aaaaaaaaa',
            'repeatNewPassword' => 'aaaaaaaaa',
            'oldPassword' => 'q1w2e3Q!W@E#',
        ]]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testNotSamePassword(): void
    {
        $authClient = $this->createAuthenticatedClient('auth_user1@example.com', 'q1w2e3Q!W@E#');
        $this->assertNotNull($authClient);

        $authClient->request('POST', '/api/security/change-password', ['json' => [
            'newPassword' => 'q1w2e3Q!W@E#11',
            'repeatNewPassword' => 'q1w2e3Q!W@E#22',
            'oldPassword' => 'q1w2e3Q!W@E#',
        ]]);

        $this->assertResponseStatusCodeSame(400);
    }
}
