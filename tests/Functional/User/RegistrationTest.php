<?php

namespace App\Tests\Functional\User;

use App\Entity\Security\EmailConfirmationToken;
use App\Entity\User;
use App\Tests\Functional\BaseApi;
use App\Tests\Functional\DatabaseTrait;

class RegistrationTest extends BaseApi
{
    use DatabaseTrait;

    public function testRegistrationSuccess(): void
    {
        static::createClient()->request('POST', '/api/users', ['json' => [
            'email' => 'test1@example.com',
            'plainPassword' => 'q1w2e3Q!W@E#',
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $user = $this->getRepositoryResult(User::class, ['email' => 'test1@example.com']);
        /** @var EmailConfirmationToken $token */
        $token = $this->getRepositoryResult(EmailConfirmationToken::class, ['user' => $user->getId()]);

        $this->assertNotNull($token);

        static::createClient()->request('GET', sprintf('/api/security/confirm-email/%s', $token->getToken()));
        $this->assertResponseIsSuccessful();
    }

    public function testDoubleEmailConfirmation(): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/users', ['json' => [
            'email' => 'test2@example.com',
            'plainPassword' => 'q1w2e3Q!W@E#',
        ]]);

        $user = $this->getRepositoryResult(User::class, ['email' => 'test2@example.com']);
        /** @var EmailConfirmationToken $token */
        $token = $this->getRepositoryResult(EmailConfirmationToken::class, ['user' => $user->getId()]);

        $this->assertNotNull($token);

        $client->request('GET', sprintf('/api/security/confirm-email/%s', $token->getToken()));
        $this->assertResponseIsSuccessful();
        $client->request('GET', sprintf('/api/security/confirm-email/%s', $token->getToken()));
        $this->assertResponseStatusCodeSame(404);
    }

    public function testEmailConfirmationWrong(): void
    {
        static::createClient()->request('GET', sprintf('/api/security/confirm-email/wrongtoken'));
        $this->assertResponseStatusCodeSame(404);
    }

    public function testRegistrationAlreadyExists(): void
    {
        static::createClient()->request('POST', '/api/users', ['json' => [
            'email' => 'auth_user1@example.com',
            'plainPassword' => 'q1w2e3Q!W@E#',
        ]]);
        
        $this->assertResponseStatusCodeSame(400);
    }
}
