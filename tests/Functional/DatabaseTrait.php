<?php

namespace App\Tests\Functional;

use Doctrine\ORM\EntityManager;

trait DatabaseTrait
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    protected function getRepository(string $class)
    {
        return $this->entityManager->getRepository($class);
    }

    protected function getRepositoryResult(string $class, array $condition, bool $isOne = true)
    {
        $repository =  $this->getRepository($class);
        if ($isOne) {
            return $repository->findOneBy($condition);
        }

        return $repository->findBy($condition);
    }
}

